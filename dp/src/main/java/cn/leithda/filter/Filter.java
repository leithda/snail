package cn.leithda.filter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:41
 * Description: 过滤器接口
 */
public interface Filter {
    <T> List<T> doFilter(List<T> list);
}

