package cn.leithda.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:41
 * Description: a字母过滤器
 */
public class ConcreteFilterA implements Filter {
    @Override
    public <T> List<T> doFilter(List<T> list) {
        final List<T> result = new ArrayList<>();
        list.forEach((item)->{
            if(!item.equals("a")){
                result.add(item);
            }
        });
        return result;
    }
}
