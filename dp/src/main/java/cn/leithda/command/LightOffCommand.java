package cn.leithda.command;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:23
 * Description: 命令
 */
public class LightOffCommand implements Command {
    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.off();
    }
}
