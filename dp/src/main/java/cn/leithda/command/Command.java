package cn.leithda.command;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:23
 * Description: 命令接口
 */
public interface Command {
    void execute();
}

