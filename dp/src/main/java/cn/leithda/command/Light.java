package cn.leithda.command;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:23
 * Description: 命令接收者
 */
public class Light {
    public void on(){
        System.out.println("Light is on!");
    }

    public void off(){
        System.out.println("Light is off!");
    }
}


