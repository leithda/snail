package cn.leithda.decorator;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:47
 * Description: 抽象装饰者
 */
public abstract class CondimentDecorator extends Beverage {

    /**
     *所有的调料装饰者都必须重新实现该方法，因为调料的该方法应该得到扩充，方法实现不同于原来方法
     */
    public abstract String getDescription();

}

