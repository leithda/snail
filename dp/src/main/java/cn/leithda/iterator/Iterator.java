package cn.leithda.iterator;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:51
 * Description: 迭代器接口
 */
public interface Iterator<E> {
    /**
     * 是否有下一个元素
     * @return 是|否
     */
    boolean hasNext();

    /**
     * 获取下一个元素
     * @return 元素
     */
    E next();
}

