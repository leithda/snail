package cn.leithda.iterator;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:51
 * Description: 词典，迭代器实现类
 */
public class Dictionary {

    private String words[] = {"hello","world","leithda"};


    public Iterator<String> iterator(){
        return new Itr();
    }

    private class Itr implements Iterator<String> {
        int cursor;
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return cursor != words.length;
        }

        @Override
        public String next() {
            int i = cursor;
            if(i >= words.length){

            }
            cursor = i+1;
            return words[lastRet = i];
        }
    }
}
