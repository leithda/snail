package cn.leithda.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:59
 * Description: 备忘录记录
 */
public class CareTaker {
    private List<Memento> mementoList = new ArrayList<Memento>();

    public void add(Memento memento){
        this.mementoList.add(memento);
    }

    public Memento get(int index){
        return mementoList.get(index);
    }
}

