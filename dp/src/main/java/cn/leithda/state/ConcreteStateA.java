package cn.leithda.state;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:13
 * Description: 具体状态1
 */
public class ConcreteStateA extends State {

    {
        desc = "休息中";
    }

    @Override
    public void handle(Context context) {
        System.out.println("当前状态" + desc);
        System.out.println("状态处理，切换工作状态");
        context.setState(new ConcreteStateB());
    }

    @Override
    public void display() {
        System.out.println("状态:" + desc);
    }
}
