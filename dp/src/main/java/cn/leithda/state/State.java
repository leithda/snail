package cn.leithda.state;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:11
 * Description: 状态抽象类
 */
public abstract class State {
    protected String desc;
    public abstract void handle(Context context);
    public abstract void display();
}
