package cn.leithda.prototype;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:09
 * Description: 原型实例1
 */
public class ConcretePrototype1 implements Prototype {
    private String name;
    public Prototype clone() {
        ConcretePrototype1 prototype = new ConcretePrototype1();
        prototype.setName(this.name);
        return prototype;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Now , This Prototype is ConcretePrototype1 , name = "+this.name;
    }
}
