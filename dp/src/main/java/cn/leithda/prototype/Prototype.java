package cn.leithda.prototype;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:09
 * Description: 原型接口
 */
public interface Prototype {
    Prototype clone();
    String getName();
    void setName(String name);
}

