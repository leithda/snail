package cn.leithda.prototype;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:10
 * Description: 原型实例2
 */
public class ConcretePrototype2 implements Prototype {
    private String name;
    public Prototype clone() {
        ConcretePrototype2 prototype = new ConcretePrototype2();
        prototype.setName(this.name);
        return prototype;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Now , This Prototype is ConcretePrototype2 , name = "+this.name;
    }
}
