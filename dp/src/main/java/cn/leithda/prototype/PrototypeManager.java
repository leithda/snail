package cn.leithda.prototype;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:10
 * Description: 原型管理器
 */
public class PrototypeManager {

    private static Hashtable<String,Prototype> map = new Hashtable<String, Prototype>();

    public static void loadPrototype(){
        ConcretePrototype1 prototype1 = new ConcretePrototype1();
        prototype1.setName("prototype1");
        map.put(prototype1.getName(),prototype1);

        ConcretePrototype2 prototype2 = new ConcretePrototype2();
        prototype2.setName("prototype2");
        map.put(prototype2.getName(),prototype2);
    }

    public static Prototype getPrototype(String prototypeId){
        Prototype prototype = map.get(prototypeId);
        return prototype.clone();
    }
}

