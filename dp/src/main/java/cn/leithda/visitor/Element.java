package cn.leithda.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:19
 * Description: 数据接口
 */
public interface Element {
    void accept(Visitor visitor);
}

