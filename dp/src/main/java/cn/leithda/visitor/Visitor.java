package cn.leithda.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:20
 * Description: 访问者接口
 */
abstract class Visitor {
    public abstract void visit(ElementA elemA);
    public abstract void visit(ElementB elemB);
}
