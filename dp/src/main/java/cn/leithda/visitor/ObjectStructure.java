package cn.leithda.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:24
 * Description: 对外提供访问
 */
public class ObjectStructure {

    private List<Element> elementList = new ArrayList<Element>();

    public void attach(Element element){
        elementList.add(element);
    }

    public void detach(Element element){
        elementList.remove(element);
    }

    public void accept(Visitor visitor){
        elementList.forEach((element)->{
            element.accept(visitor);
        });
    }
}
