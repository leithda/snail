package cn.leithda.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:23
 * Description: 访问者1
 */
public class OneVisitor extends Visitor {
    @Override
    public void visit(ElementA elemA) {
        System.out.println("访问者 OneVisitor 访问 ElementA");
    }

    @Override
    public void visit(ElementB elemB) {
        System.out.println("访问者 OneVisitor 访问 ElementB");
    }
}
