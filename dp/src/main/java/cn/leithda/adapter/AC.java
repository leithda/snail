package cn.leithda.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:33
 * Description: 电压接口
 */
public interface AC {
    /**
     * 输出电压
     * @return 电压值
     */
    int outputAC();
}
