package cn.leithda.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:33
 * Description: 110伏电压
 */
public class AC110 implements AC {

    @Override
    public int outputAC() {
        int ac = 110;
        return ac;
    }
}
