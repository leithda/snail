package cn.leithda.adapter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:35
 * Description: 适配器使用者，充电器类
 */
public class Charger {

    private List<DC5Adapter> adapters = new LinkedList<>();

    public Charger() {
        this.adapters.add(new ChinaPowerAdapter());
        this.adapters.add(new JapanPowerAdapter());
    }

    /**
     * 转换电压
     * @param ac 输入电压
     * @return 输出电压
     */
    public int convertAC(AC ac) {
        DC5Adapter adapter = null;
        for (DC5Adapter ad : this.adapters) {
            if (ad.support(ac)) {
                adapter = ad;
                break;
            }
        }
        if (adapter == null){
            throw new  IllegalArgumentException("没有找到合适的变压适配器");
        }
        return adapter.outputDC5V(ac);
    }
}
