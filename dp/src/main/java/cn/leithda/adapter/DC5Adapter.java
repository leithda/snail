package cn.leithda.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:34
 * Description: 适配器接口
 */
public interface DC5Adapter {

    /**
     * 是否可以适配
     * @param ac 电压
     * @return 是否
     */
    boolean support(AC ac);

    /**
     * 适配处理
     * @param ac 输入电压
     * @return 适配后电压
     */
    int outputDC5V(AC ac);
}
