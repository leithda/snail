package cn.leithda.factory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:46
 * Description: Office工厂类
 */
public class OfficeFactory {
    /**
     * 获取 Office 组件
     *
     * @param type 组件类型
     * @return 组件实例
     */
    public static Office getOffice(OfficeType type) {
        if (type == null) {
            return null;
        }
        switch (type) {
            case WORD:
                return new Word();
            case EXCEL:
                return new Excel();
            case PPT:
                return new PowerPoint();
            default:
                throw new RuntimeException("未知参数，无法创建对应实例");
        }
    }
}
