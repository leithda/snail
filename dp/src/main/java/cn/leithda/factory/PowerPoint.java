package cn.leithda.factory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:45
 * Description: PowerPoint
 */
public class PowerPoint implements Office{

    public void bootstrap() {
        System.out.println("PowerPoint 启动 <<<<<<<<<<<<");
        try{
            System.out.println("图形化界面初始化...");
            Thread.sleep(300);
            System.out.println("渲染进行中...");
            Thread.sleep(700);
        }catch (Exception e){
            System.out.println("PowerPoint 启动失败!!\n\n");
            e.printStackTrace();
        }
        System.out.println("PowerPoint 启动成功 <<<<<<<<<<<<");
    }
}
