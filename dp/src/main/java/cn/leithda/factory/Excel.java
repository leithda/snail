package cn.leithda.factory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:45
 * Description: Excel
 */
public class Excel implements Office {

    public void bootstrap() {
        System.out.println("Excel 启动 <<<<<<<<<<<<");
        try{
            System.out.println("加载系统配置...");
            Thread.sleep(500);
            System.out.println("加载用户变量...");
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("Excel 启动失败!!\n\n");
            e.printStackTrace();
        }

        System.out.println("Excel 启动完成 <<<<<<<<<<<<");
    }
}
