package cn.leithda.factory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:45
 * Description: Office类型枚举
 */
public enum OfficeType {
    WORD,
    EXCEL,
    PPT
}
