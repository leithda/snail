package cn.leithda.factory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:44
 * Description: Office 接口
 */
public interface Office {
    /**
     * 启动方法
     */
    void bootstrap();
}
