package cn.leithda.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:54
 * Description: 红色
 */
public class Red implements Color {

    public void fill() {
        System.out.println("Inside Red::fill() method.");
    }

}
