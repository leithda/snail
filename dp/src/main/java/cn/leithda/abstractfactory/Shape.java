package cn.leithda.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:52
 * Description: 形状接口
 */
public interface Shape {
    void draw();
}
