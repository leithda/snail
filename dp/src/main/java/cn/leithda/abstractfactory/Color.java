package cn.leithda.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:53
 * Description: 颜色接口
 */
public interface Color {
    void fill();
}
