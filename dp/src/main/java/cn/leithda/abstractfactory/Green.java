package cn.leithda.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:54
 * Description: 绿色
 */
public class Green implements Color {

    public void fill() {
        System.out.println("Inside Green::fill() method.");
    }
}
