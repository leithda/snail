package cn.leithda.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:54
 * Description: 圆形
 */
public class Square implements Shape {

    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
