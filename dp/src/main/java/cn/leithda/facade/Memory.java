package cn.leithda.facade;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:50
 * Description: Memory
 */
public class Memory {
    public void startup(){
        System.out.println("memory startup!");
    }
    public void shutdown(){
        System.out.println("memory shutdown!");
    }
}
