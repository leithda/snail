package cn.leithda.facade;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:49
 * Description: CPU
 */
public class CPU {
    public void startup(){
        System.out.println("cpu startup!");
    }
    public void shutdown(){
        System.out.println("cpu shutdown!");
    }
}
