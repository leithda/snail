package cn.leithda.facade;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:50
 * Description: Disk
 */
public class Disk {
    public void startup(){
        System.out.println("disk startup!");
    }
    public void shutdown(){
        System.out.println("disk shutdown!");
    }
}
