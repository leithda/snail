package cn.leithda.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:39
 * Description: TV实现类
 */
public class Sony extends TV {
    @Override
    public void on() {
        System.out.println("Sony on ");
    }

    @Override
    public void off() {
        System.out.println("Sony off ");
    }

    @Override
    public void tuneChannel() {
        System.out.println("Sony.tuneChannel ");
    }
}
