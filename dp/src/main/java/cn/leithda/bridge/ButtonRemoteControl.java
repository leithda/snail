package cn.leithda.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:39
 * Description: 按键遥控器类
 */
public class ButtonRemoteControl extends RemoteControl {
    public ButtonRemoteControl(TV tv) {
        super(tv);
    }

    @Override
    public void on() {
        System.out.println("按键遥控器.on()");
        tv.on();
    }

    @Override
    public void off() {
        System.out.println("按键遥控器.off()");
        tv.off();
    }

    @Override
    public void tuneChannel() {
        System.out.println("按键遥控器.tuneChannel()");
        tv.tuneChannel();
    }
}
