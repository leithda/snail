package cn.leithda.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:38
 * Description: 电视抽象类
 */
public abstract class TV {
    public abstract void on();

    public abstract void off();

    public abstract void tuneChannel();
}
