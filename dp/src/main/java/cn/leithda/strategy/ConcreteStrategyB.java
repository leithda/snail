package cn.leithda.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:16
 * Description: 策略B
 */
public class ConcreteStrategyB extends Strategy {
    @Override
    public void behavior() {
        System.out.println("策略B");
    }
}
