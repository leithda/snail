package cn.leithda.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:16
 * Description: 策略A
 */
public class ConcreteStrategyA extends Strategy {
    @Override
    public void behavior() {
        System.out.println("策略A");
    }
}
