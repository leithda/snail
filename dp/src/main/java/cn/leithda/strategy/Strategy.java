package cn.leithda.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:15
 * Description: 策略接口
 */
public abstract class Strategy {
    // 算法方法
    public abstract void behavior();
}
