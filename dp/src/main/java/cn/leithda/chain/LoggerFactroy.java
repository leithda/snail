package cn.leithda.chain;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:15
 * Description: 日志工厂
 */
public class LoggerFactroy {
    private LoggerFactroy(){}

    public static AbstractLogger getLogger(){
        AbstractLogger logger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

        logger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);
        return logger;
    }
}
