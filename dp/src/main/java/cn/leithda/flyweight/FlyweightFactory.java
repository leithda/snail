package cn.leithda.flyweight;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:53
 * Description: 享元工厂类
 */
public class FlyweightFactory {

    private HashMap<String,Flyweight> flyweightHashMap = new HashMap<>();


    Flyweight getFlyweight(String intrinsicState){
        if(!flyweightHashMap.containsKey(intrinsicState)){
            Flyweight flyweight = new ConcreteFlyweight(intrinsicState);
            flyweightHashMap.put(intrinsicState,flyweight);
        }
        return flyweightHashMap.get(intrinsicState);
    }
}

