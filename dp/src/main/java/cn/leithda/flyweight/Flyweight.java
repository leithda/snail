package cn.leithda.flyweight;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:52
 * Description: 享元接口
 */
public interface Flyweight  {
    void doOperation(String extrinsicState);
}

