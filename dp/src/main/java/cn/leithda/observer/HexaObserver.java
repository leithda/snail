package cn.leithda.observer;

import cn.leithda.observer.Observer;
import cn.leithda.observer.Subject;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:04
 * Description: 十六进制观察者
 */
public class HexaObserver  extends Observer {

    public HexaObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("Binary String: "
                + Integer.toHexString(subject.getState()));
    }
}
