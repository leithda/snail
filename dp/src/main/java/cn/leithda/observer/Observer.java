package cn.leithda.observer;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:02
 * Description: 观察者接口
 */
public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
