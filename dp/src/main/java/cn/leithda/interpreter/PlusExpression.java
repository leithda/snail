package cn.leithda.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:26
 * Description: 加法操作
 */
public class PlusExpression extends NonTerminalExpression{
    public PlusExpression(Expression e1, Expression e2) {
        super(e1, e2);
    }


    @Override
    public int interpret() {
        return  this.e1.interpret() + this.e2.interpret();
    }
}

