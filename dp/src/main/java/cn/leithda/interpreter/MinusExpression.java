package cn.leithda.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:27
 * Description: 减法操作
 */
public class MinusExpression extends NonTerminalExpression {
    public MinusExpression(Expression e1, Expression e2) {
        super(e1, e2);
    }

    @Override
    public int interpret() {
        return this.e1.interpret() - this.e2.interpret();
    }

}

