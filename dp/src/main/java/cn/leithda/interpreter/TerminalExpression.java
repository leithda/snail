package cn.leithda.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:27
 * Description: 终结表达式
 */
public class TerminalExpression implements Expression{

    String value;

    public TerminalExpression(String value) {
        this.value = value;
    }

    @Override
    public int interpret() {
        return Integer.parseInt(value);
    }
}

