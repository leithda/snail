package cn.leithda.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:26
 * Description: 表达式接口
 */
public interface Expression {
    public int interpret();
}

