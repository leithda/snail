package cn.leithda.interpreter;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:28
 * Description: 表达式解析器
 */
public final class ExpressionParser {

    public static Expression parse(String expr){
        Expression expression = null;
        for(int i=0;i<expr.length();i++){
            char ch =  expr.charAt(i);
            switch(ch){
                case '+':
                    expression = new PlusExpression(expression,new TerminalExpression(String.valueOf(expr.charAt(++i))));
                    break;
                case '-':
                    expression = new MinusExpression(expression,new TerminalExpression(String.valueOf(expr.charAt(++i))));
                    break;
                default:
                    expression = new TerminalExpression(String.valueOf(ch));
            }
        }
        return expression;
    }
}
