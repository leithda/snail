package cn.leithda.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:53
 * Description: 中介者抽象类
 */
abstract class Mediator {
    public abstract void processMessage(Message message);

    public abstract void addUser(User user);

    public abstract void addFileServer(FileServer fileServer);
}

