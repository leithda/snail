package cn.leithda.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:53
 * Description: 消息
 */
public class Message {

    /**
     * 消息来源
     */
    String from;
    /**
     * 消息内容
     */
    String content;
    /**
     * 消息接收方
     */
    String to;
    /**
     * 消息类型
     */
    String type;

    public Message(String type) {
        this.type = type;
    }

    public Message setFrom(String from){
        this.from = from;
        return this;
    }

    public Message setContent(String content) {
        this.content = content;
        return this;
    }

    public Message setTo(String to){
        this.to = to;
        return this;
    }

    public String getFrom() {
        return from;
    }

    public String getContent() {
        return content;
    }

    public String getTo() {
        return to;
    }

    public String getType(){
        return type;
    }
}

