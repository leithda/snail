package cn.leithda.mediator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:55
 * Description: 中介者实现类
 */
public class ConcreteMediator extends Mediator {

    List<User> userList = new ArrayList<>();
    List<FileServer> fileServerList = new ArrayList<>();

    public ConcreteMediator() {
    }


    @Override
    public void processMessage(Message message) {
        String messageType = message.getType();
        String content = message.getContent();
        String from = message.getFrom();
        String to = message.getTo();
        switch (messageType) {
            case "file":
                fileServerList.forEach((FileServer server) -> {
                    if (content.startsWith("U")) {
                        String fileName = content.substring(1);
                        server.upload(fileName, from);
                    } else if (content.startsWith("D")) {
                        String fileName = content.substring(1);
                        server.download(fileName, from);
                    } else {
                        this.processMessage(new Message("msg").setContent("请求内容错误").setTo(from).setFrom("系统"));
                    }
                });
                break;
            case "msg":
                if (to == null || to.equals("")) {
                    userList.forEach((User u) -> {
                        if(!u.getName().equals(from)) {
                            String msg = from + ": " + content;
                            u.putMessage(msg);
                        }
                    });
                }else{
                    userList.forEach((User u)->{
                        if(u.getName().equals(to)){
                            u.putMessage(from + ": "+content);
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void addUser(User user) {
        this.userList.add(user);
    }

    @Override
    public void addFileServer(FileServer fileServer) {
        this.fileServerList.add(fileServer);
    }
}

