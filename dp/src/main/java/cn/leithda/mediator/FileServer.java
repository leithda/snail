package cn.leithda.mediator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:54
 * Description: 文件服务器
 */
public class FileServer {
    List<String> fileList = new ArrayList<>();

    private Mediator mediator;

    public FileServer(Mediator mediator) {
        this.mediator = mediator;
        this.mediator.addFileServer(this);
    }

    private boolean isFound;

    public void upload(String fileName, String userName) {
        String returnContent;
        if (!fileList.contains(fileName)) {
            fileList.add(fileName);
            returnContent = "文件上传成功";
        } else {
            returnContent = "文件已经存在";
        }
        Message message = new Message("msg").setFrom("FileServer").setContent(returnContent).setTo(userName);
        mediator.processMessage(message);
    }


    public void download(String fileName, String userName) {
        String returnContent;
        isFound = false;
        fileList.forEach((String file) -> {
            if (file.equalsIgnoreCase(fileName)) {
                isFound = true;
            }
        });
        if (isFound) {
            returnContent = "[" + fileName + "]传输中...";
        } else {
            returnContent = "下载失败，[" + fileName + "]不存在";
        }
        Message message = new Message("msg").setFrom("FileServer").setContent(returnContent).setTo(userName);
        mediator.processMessage(message);
    }

    public void showFileList() {
        fileList.forEach(System.out::println);
    }
}


