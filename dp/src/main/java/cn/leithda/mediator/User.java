package cn.leithda.mediator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:54
 * Description: 用户类
 */
public class User {

    /**
     * 客户名称
     */
    private String name;

    /**
     * 消息列表
     */
    private List<String> messageList = new ArrayList<>();

    /**
     * 中介者类
     */
    private Mediator mediator;

    public User(Mediator mediator) {
        this.mediator = mediator;
        this.mediator.addUser(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 发送消息
     *
     * @param content 消息
     * @param to 消息接收方
     */
    public void pushMessage(String content, String to) {
        content = content + "\t\t" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd hh:mm:ss"));
        this.messageList.add("me :" + content);
        Message message = new Message("msg").setContent(content).setFrom(getName()).setTo(to);
        this.mediator.processMessage(message);
    }

    /**
     * 上传文件
     * @param content 文件名称
     */
    public void uploadFile(String content){
        Message message = new Message("file").setFrom(getName()).setContent("U"+content);
        this.mediator.processMessage(message);
    }

    /**
     * 下载文件
     * @param content 文件名称
     */
    public void downloadFile(String content){
        this.messageList.add("申请下载《"+content+"》，请求文件服务器中...");
        Message message = new Message("file").setFrom(getName()).setContent("D"+content);
        this.mediator.processMessage(message);
    }

    /**
     * 接收消息
     * @param content 消息
     */
    protected void putMessage(String content) {
        this.messageList.add(content);
    }

    /**
     * 展示消息列表
     */
    public void showMessages() {
        System.out.println("\n======== " + getName() + "'s messageBox begin ========");
        messageList.forEach(System.out::println);
        System.out.println("======== " + getName() + "'s messageBox  end  ========\n");
    }
}
