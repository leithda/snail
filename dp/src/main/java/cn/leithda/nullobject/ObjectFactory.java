package cn.leithda.nullobject;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:29
 * Description: 对象工厂
 */
public class ObjectFactory {

    public static AbstractObject getObject(String type){
        if(type != null && !"".equals(type)){
            return new RealObject();
        }else{
            return new NullObject();
        }
    }
}
