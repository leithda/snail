package cn.leithda.nullobject;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:28
 * Description: 实际对象
 */
public class RealObject extends AbstractObject {
    @Override
    public void request() {
        System.out.println("do something ");
    }

    @Override
    public boolean isNil() {
        return false;
    }
}
