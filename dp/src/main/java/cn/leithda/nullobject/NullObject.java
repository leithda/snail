package cn.leithda.nullobject;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:28
 * Description: 空对象
 */
public class NullObject extends AbstractObject {
    @Override
    public void request() {
        // do nothing
    }

    @Override
    public boolean isNil() {
        return true;
    }
}
