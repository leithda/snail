package cn.leithda.nullobject;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:28
 * Description: 抽象对象
 */
public abstract class AbstractObject {
    public abstract void request();
    public abstract boolean isNil();
}

