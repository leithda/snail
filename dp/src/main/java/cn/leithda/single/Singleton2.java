package cn.leithda.single;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:20
 * Description: 单例模式2，懒汉式，线程安全，Lazy加载，性能低
 */
public class Singleton2 {
    private static Singleton2 instance;

    private Singleton2() {
    }

    /**
     *  加锁 synchronized  保证单例，影响效率
     */
    public static synchronized Singleton2 getInstance() {
        if (instance == null) {
            instance = new Singleton2();
        }
        return instance;
    }
}
