package cn.leithda.single;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:21
 * Description: 单例模式4，双重锁验锁（DCL，即 double-checked locking）,线程安全，Lazy加载，性能较高，实现复杂
 */
public class Singleton4 {
    /**
     * 增加volatile是为了避免代码优化时进行重排序，导致返回未初始化成功的对象
     */
    private volatile static Singleton4 instance;

    private Singleton4(){}

    public static Singleton4 getInstance(){
        // 当对象没有加载时，拿锁进行对象实例化保证单例。对象加载时，直接返回，性能较好
        if(instance == null){
            synchronized (Singleton4.class){
                if(instance == null){
                    instance = new Singleton4();
                }
            }
        }
        return instance;
    }
}
