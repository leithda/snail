package cn.leithda.single;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:09
 * Description: 单例模式1，懒汉式，线程不安全，Lazy加载
 */
public class Singleton {
    private static Singleton instance;

    /**
     * 设置构造函数为私有，防止类被实例化
     */
    private Singleton(){}
    /* 可以通过下面代码避免反射多次实例化 单例对象
    private Singleton(){
        if(instance != null){
            throw new IllegalArgumentException("单例构造器不能重复使用");
        }
    }
    */

    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
}
