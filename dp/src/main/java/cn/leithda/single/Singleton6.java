package cn.leithda.single;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:22
 * Description: 单例模式6
 */
public enum Singleton6 {
    // 这种方式可以防止通过 reflection attact 调用类的私有构造方法
    INSTANCE;
}
