package cn.leithda.single;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:21
 * Description: 单例模式5，内部类实现，线程安全，Lazy加载
 */
public class Singleton5 {
    /**
     * 优化了Singleton3，使用静态内部类实现了Lazy加载
     */
    private static class SingleHolder{
        private static final Singleton5 INSTANCE  = new Singleton5();
    }
    private Singleton5(){}

    public static Singleton5 getInstance(){
        return SingleHolder.INSTANCE;
    }
}
