package cn.leithda.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:04
 * Description: 电脑，具体产品类
 */
public class Computer {
    List<String> components = new ArrayList<String>();

    public void add(String component){
        components.add(component);
    }

    public void display(){
        System.out.println("个人电脑，组成如下:\n--------------");
        components.forEach((System.out::println));
    }
}
