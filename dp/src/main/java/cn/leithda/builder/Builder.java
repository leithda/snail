package cn.leithda.builder;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:05
 * Description: 抽象建造者
 */
abstract class Builder {
    public abstract void buildCPU();
    public abstract void buildGPU();
    public abstract void buildPower();
    public abstract Computer build();
}
