package cn.leithda.builder;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:06
 * Description: 指挥者
 */
public class Director {

    public void construct(Builder builder){
        builder.buildCPU();
        builder.buildGPU();
        builder.buildPower();
    }
}
