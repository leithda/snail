package cn.leithda.builder;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:05
 * Description: 标准建造者
 */
public class StandardComputer extends Builder {

    private Computer computer = new Computer();

    @Override
    public void buildCPU() {
        computer.add("处理器");
    }

    @Override
    public void buildGPU() {
        computer.add("显卡");
    }

    @Override
    public void buildPower() {
        computer.add("电源");
    }

    @Override
    public Computer build() {
        return computer;
    }
}
