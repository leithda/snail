package cn.leithda.builder;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:05
 * Description: 自定义建造者
 */
public class MyComputer extends Builder{

    private Computer computer = new Computer();

    @Override
    public void buildCPU() {
        computer.add("AMD 2600x");
    }

    @Override
    public void buildGPU() {
        computer.add("GTX 2060");
    }

    @Override
    public void buildPower() {
        computer.add("爱国者 1250w");
    }

    @Override
    public Computer build() {
        return computer;
    }
}
