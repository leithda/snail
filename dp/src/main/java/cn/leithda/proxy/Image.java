package cn.leithda.proxy;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:55
 * Description: 代理接口
 */
public interface Image {
    void showImage();
}
