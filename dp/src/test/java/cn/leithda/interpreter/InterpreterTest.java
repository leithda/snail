package cn.leithda.interpreter;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:28
 * Description: 解析器模式测试
 */
public class InterpreterTest {

    @Test
    public void test() throws Exception {
        TerminalExpression s3 = new TerminalExpression("333");
        TerminalExpression s5 = new TerminalExpression("555");
        TerminalExpression s8 = new TerminalExpression("888");

        int result = new MinusExpression(new PlusExpression(s3,s8),s5).interpret();
        System.out.println(result);
    }

    @Test
    public void test2() throws Exception{
        Expression exp = ExpressionParser.parse("3+8-5");
        int result = exp.interpret();
        System.out.println(result);
    }
}
