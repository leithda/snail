package cn.leithda.mediator;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:55
 * Description: 中介者模式测试类
 */
public class MediatorTest {

    @Test
    public void test() throws Exception{
        Mediator mediator = new ConcreteMediator();

        FileServer fileServer = new FileServer(mediator);

        User u1 = new User(mediator);
        User u2 = new User(mediator);
        User u3 = new User(mediator);
        u1.setName("leithda");
        u2.setName("mellofly");
        u3.setName("吃瓜群众");

        u3.uploadFile("三国演义");
        u2.uploadFile("设计模式");

        u1.pushMessage("Hello, mellofly, Where are you?","mellofly");
        u2.pushMessage("Hi，Le，我在上海，我上传了一个文件叫《设计模式》","leithda");

        u1.pushMessage("大家，mellofly上传了《设计模式》，大家快去看啊","");

        u1.downloadFile("设计模式");
        u2.downloadFile("三国演绎");
        u2.downloadFile("三国演义");
        u3.downloadFile("设计模式");

        u1.showMessages();
        u2.showMessages();
        u3.showMessages();

        fileServer.showFileList();
    }
}
