package cn.leithda.bridge;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:40
 * Description: 桥接模式测试
 */
public class BridgeTest {

    @Test
    public void test() throws Exception{
        ButtonRemoteControl buttonRemoteControl = new ButtonRemoteControl(new RCA());
        buttonRemoteControl.on();
        buttonRemoteControl.off();
        buttonRemoteControl.tuneChannel();

        VoiceRemoteControl voiceRemoteControl = new VoiceRemoteControl(new Sony());
        voiceRemoteControl.on();
        voiceRemoteControl.off();
        voiceRemoteControl.tuneChannel();
    }
}
