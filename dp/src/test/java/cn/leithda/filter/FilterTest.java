package cn.leithda.filter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:42
 * Description: 过滤器模式测试
 */
public class FilterTest {

    List<String> stringList = new ArrayList<>();
    List<Filter> filterList = new ArrayList<>();

    @Before
    public void init(){
        stringList.add("a");
        stringList.add("b");
        stringList.add("c");
        stringList.add("d");

        filterList.add(new ConcreteFilterA());
        filterList.add(new ConcreteFilterB());
    }

    @Test
    public void test() throws Exception{
        filterList.forEach((filter -> {
            List<String> result = filter.doFilter(stringList);
            System.out.println("Filter"+filter.getClass().getSimpleName()+" : "+result.toString());
        }));
    }
}

