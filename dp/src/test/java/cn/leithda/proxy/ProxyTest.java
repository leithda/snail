package cn.leithda.proxy;

import org.junit.Test;

import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:56
 * Description: 代理模式测试
 */
public class ProxyTest {

    @Test
    public void test() throws Exception{
        String image = "http://image.jpg";
        URL url = new URL(image);
        HighResolutionImage highResolutionImage = new HighResolutionImage(url);
        ImageProxy imageProxy = new ImageProxy(highResolutionImage);
        imageProxy.showImage();
    }
}

