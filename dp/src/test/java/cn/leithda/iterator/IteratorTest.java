package cn.leithda.iterator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:51
 * Description: 迭代器模式测试
 */
public class IteratorTest {

    @Test
    public void test(){
        Dictionary dictionary = new Dictionary();

        Iterator it = dictionary.iterator();

        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}