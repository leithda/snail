package cn.leithda.factory;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 14:46
 * Description: 工厂模式测试
 */
public class OfficeFactoryTest {

    @Test
    public void test(){
        Office word = OfficeFactory.getOffice(OfficeType.WORD);
        Office excel = OfficeFactory.getOffice(OfficeType.EXCEL);
        Office powerPoint = OfficeFactory.getOffice(OfficeType.PPT);
        word.bootstrap();
        excel.bootstrap();
        powerPoint.bootstrap();
    }
}
