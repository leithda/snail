package cn.leithda.visitor;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:25
 * Description: 访问者模式测试
 */
public class VisitorTest {

    @Test
    public void test(){
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.attach(new ElementA());
        objectStructure.attach(new ElementB());

        objectStructure.accept(new OneVisitor());
        objectStructure.accept(new TwoVisitor());
    }
}
