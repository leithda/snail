package cn.leithda.nullobject;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:29
 * Description: 空对象模式测试
 */
public class NullObjectTest {
    @Test
    public void test() throws Exception {

        AbstractObject real = ObjectFactory.getObject("real");
        if (!real.isNil()) {
            real.request();
        }

        AbstractObject nul = ObjectFactory.getObject("");
        if (!nul.isNil()) {
            nul.request();
        }
    }
}
