package cn.leithda.facade;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:51
 * Description: 外观模式测试
 */
public class FacadeTest {

    @Test
    public void testBootstrap() throws Exception{
        Computer computer = new Computer();
        computer.startup();
    }

    @Test
    public void testShutdown() throws  Exception{
        Computer computer = new Computer();
        computer.shutdown();
    }

}
