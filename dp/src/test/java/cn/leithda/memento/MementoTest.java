package cn.leithda.memento;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:00
 * Description: 备忘录模式测试
 */
public class MementoTest {

    @Test
    public void test() throws Exception {

        CareTaker careTaker = new CareTaker();

        // 初始化并建立备忘录
        Originator o = new Originator();
        o.setState("init");
        o.show();
        careTaker.add(o.createMemento());

        // 状态错误
        o.setState("error");
        o.show();

        // 从备忘录中恢复
        o.setMemento(careTaker.get(0));
        o.show();

    }

}

