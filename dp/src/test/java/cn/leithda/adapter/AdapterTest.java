package cn.leithda.adapter;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:35
 * Description: 适配器模式测试
 */
public class AdapterTest {

    /**
     * 输出如下：
     *     使用ChinaPowerAdapter变压适配器，输入AC:220V，输出DC:5V
     *     220V电压 充电 转换后的电压为:5
     *     使用JapanPowerAdapter变压适配器，输入AC:110V，输出DC:5V
     *     110V电压 充电 转换后的电压为:5
     */
    @Test
    public void testCharger() throws Exception{

        AC chinaAC = new AC220();
        Charger charger = new Charger();
        System.out.println("220V电压 充电 转换后的电压为:"+charger.convertAC(chinaAC));


        // 去日本旅游，电压是 110V
        AC japanAC = new AC110();
        System.out.println("110V电压 充电 转换后的电压为:"+charger.convertAC(japanAC));
    }
}