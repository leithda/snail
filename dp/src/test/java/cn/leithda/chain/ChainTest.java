package cn.leithda.chain;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:15
 * Description: 责任链模式测试
 */
public class ChainTest {

    @Test
    public void test() throws Exception {
        AbstractLogger logger = LoggerFactroy.getLogger();
        logger.logMessage(AbstractLogger.INFO, "This is an information.");

        logger.logMessage(AbstractLogger.DEBUG,
                "This is a debug level information.");

        logger.logMessage(AbstractLogger.ERROR,
                "This is an error information.");
    }
}
