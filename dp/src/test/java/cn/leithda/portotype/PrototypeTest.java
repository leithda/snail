package cn.leithda.portotype;

import cn.leithda.prototype.Prototype;
import cn.leithda.prototype.PrototypeManager;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:11
 * Description: 测试原型模式
 */
public class PrototypeTest {

    @Test
    public void test() {
        PrototypeManager.loadPrototype();
        Prototype p1 = PrototypeManager.getPrototype("prototype1");
        System.out.println(p1);
        Prototype p2 = PrototypeManager.getPrototype("prototype2");
        System.out.println(p2);
    }
}

