package cn.leithda.observer;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:07
 * Description: 观察者模式测试
 */
public class ObserverTest {

    @Test
    public void test(){
        Subject subject = new Subject();

        new BinaryObserver(subject);    // 二进制
        new OctalObserver (subject);    // 八进制
        new HexaObserver(subject);  // 十六进制

        System.out.println("First state change: 15");
        subject.setState(15);

        System.out.println("Second state change: 10");
        subject.setState(10);
    }
}
