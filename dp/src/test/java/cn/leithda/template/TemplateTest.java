package cn.leithda.template;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:18
 * Description: 模板模式测试
 */
public class TemplateTest {

    @Test
    public void test() throws Exception{
        Game game = new Cricket();
        game.play();
        System.out.println();
        game = new Football();
        game.play();
    }
}
