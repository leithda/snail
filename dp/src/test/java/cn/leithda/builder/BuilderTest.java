package cn.leithda.builder;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 15:06
 * Description: 建造者测试
 */
public class BuilderTest {

    @Test
    public void test() {
        Director director = new Director();

        Builder standardBuilder = new StandardComputer();
        Builder myBuilder = new MyComputer();

        director.construct(standardBuilder);
        Computer standardComputer = standardBuilder.build();
        standardComputer.display();

        director.construct(myBuilder);
        Computer myComputer = myBuilder.build();
        myComputer.display();
    }
}
