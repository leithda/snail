package cn.leithda.state;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:13
 * Description: 状态模式测试
 */
public class StateTest {

    @Test
    public void test() throws Exception {
        Context context = new Context(new ConcreteStateA());
        context.displayState();
        context.request();
        context.displayState();
        context.request();
        context.displayState();
    }
}
