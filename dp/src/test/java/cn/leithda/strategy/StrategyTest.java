package cn.leithda.strategy;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:17
 * Description: 策略模式测试
 */
public class StrategyTest {

    @Test
    public void test() throws Exception{
        Context a = new Context(new ConcreteStrategyA());
        a.doSomething();

        Context b = new Context(new ConcreteStrategyB());
        b.doSomething();
    }
}
