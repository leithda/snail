package cn.leithda.compusite;

import cn.leithda.composite.Component;
import cn.leithda.composite.Composite;
import cn.leithda.composite.Leaf;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 16:45
 * Description: 组合模式测试
 */
public class CompositeTest {

    @Test
    public void test() throws Exception {
        Composite root = new Composite("root");
        Component node1 = new Leaf("1");
        Component node2 = new Composite("2");
        Component node3 = new Leaf("3");
        root.add(node1);
        root.add(node2);
        root.add(node3);
        Component node21 = new Leaf("21");
        Component node22 = new Composite("22");
        node2.add(node21);
        node2.add(node22);
        Component node221 = new Leaf("221");
        node22.add(node221);
        root.print();
    }
}
