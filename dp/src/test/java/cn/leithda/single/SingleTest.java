package cn.leithda.single;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: leithda
 * Date: 2021/5/27
 * Time: 13:10
 * Description: 单例模式测试
 */
public class SingleTest {


    @Test
    public void test() {
        Singleton singletonFirst = Singleton.getInstance();
        Singleton singletonSecond = Singleton.getInstance();
        System.out.println("singleton==singleton1 is " + (singletonFirst == singletonSecond));

        Singleton2 singleton2First = Singleton2.getInstance();
        Singleton2 singleton2Second = Singleton2.getInstance();
        System.out.println("singleton2First==singleton2Second is " + (singleton2First == singleton2Second));


        Singleton3 singleton3First = Singleton3.getInstance();
        Singleton3 singleton3Second = Singleton3.getInstance();
        System.out.println("singleton3First==singleton3Second is " + (singleton3First == singleton3Second));


        Singleton4 singleton4First = Singleton4.getInstance();
        Singleton4 singleton4Second = Singleton4.getInstance();
        System.out.println("singleton4First==singleton4Second is " + (singleton4First == singleton4Second));

        Singleton5 singleton5First = Singleton5.getInstance();
        Singleton5 singleton5Second = Singleton5.getInstance();
        System.out.println("singleton5First==singleton5Second is " + (singleton5First == singleton5Second));

        Singleton6 singleton6First = Singleton6.INSTANCE;
        Singleton6 singleton6Second = Singleton6.INSTANCE;
        System.out.println("singleton6First==singleton6Second is " + (singleton6First == singleton6Second));
    }
}
